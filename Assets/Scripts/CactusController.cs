﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CactusController : MonoBehaviour {

	Animator anim;
	Rigidbody rb;
	int attackHash = Animator.StringToHash("Attack");
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		float move = Input.GetAxis ("Vertical");
		anim.SetFloat ("Speed", Mathf.Abs(move));

		if (Input.GetKeyDown(KeyCode.Space)){
			anim.SetTrigger(attackHash);
		}
		Vector3 movement = new Vector3 (0.0f, 0.0f, move * 100);
		rb.velocity = movement;

	}
}
